package demo;

/**
 * @author Javen
 */
public class Test implements A, B {
    public static void main(String[] args) {
        System.out.println(a);
    }
}

interface A {
    int a = 10;
}

interface B {

}

interface C extends A, B {
}


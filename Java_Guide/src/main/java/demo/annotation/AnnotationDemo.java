package demo.annotation;

import cn.hutool.json.JSONUtil;
import demo.entity.OpenSourceEntity;

import java.lang.reflect.Field;

/**
 * 注解示例
 * @author Javen
 */
public class AnnotationDemo {

    @OpenSource
    OpenSourceEntity openSource;

    public OpenSourceEntity getOpenSource() {
        return openSource;
    }

    public void setOpenSource(OpenSourceEntity openSource) {
        this.openSource = openSource;
    }

    public static void annotationProcess(AnnotationDemo annotationDemo) {
        for (Field field : annotationDemo.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(OpenSource.class)) {
                OpenSource openSource = field.getAnnotation(OpenSource.class);
                annotationDemo.setOpenSource(
                        new OpenSourceEntity(openSource.author(), openSource.name(), openSource.url())
                );
            }
        }
    }

    public static void main(String[] args) {
        AnnotationDemo annotationDemo = new AnnotationDemo();
        annotationProcess(annotationDemo);
        System.out.println(JSONUtil.toJsonStr(annotationDemo));
    }
}

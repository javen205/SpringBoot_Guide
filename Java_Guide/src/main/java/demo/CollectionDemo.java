package demo;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * 故障现象
 * java.util.ConcurrentModificationException
 * 导致原因
 * <p>
 * 解决方法
 * new Vector();
 * Collections.synchronizedList(new ArrayList<>())
 * new CopyOnWriteArrayList<>()
 * 优化建议
 *
 *
 * HashMap ConcurrentHashMap
 * https://blog.csdn.net/weixin_44460333/article/details/86770169
 * @author Javen
 */
@Slf4j
public class CollectionDemo {
    public static void main(String[] args) {
        final List<Object> list = Collections.synchronizedList(new ArrayList<>());

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                log.info("list: {}", list);
            }, String.valueOf(i)).start();
        }
    }
}




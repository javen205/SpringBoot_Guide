package demo;

/**
 * https://blog.csdn.net/noaman_wgs/article/details/74489549
 *
 * 类的加载过程是：
 * （1）加载B类；
 * （2）验证、准备（给静态属性t1、t2赋默认值null）、解析；
 * （3）初始化：执行public static B t1 = new B(); 和 public static B t2 = new B();、static {}；
 * 注意，我们之前说的类的加载顺序是 有父类先加载父类静态域，这里的静态域不仅包括static{} 代码块，还有 静态属性；
 * 然后再执行构造块{}和构造方法；
 * 所以在执行public static B t1 = new B();时，不会加载static{}代码块，否则就会重复执行，只会执行B的构造块和构造方法(默认的无参构造器)，输出"构造块"；
 * 执行public static B t2 = new B();时，也同样执行构造块和构造方法，输出"构造块"；
 * 第三步再执行static{} 代码块，输出”静态快“；
 * 最后执行main方法中的B t = new B();，输出"构造块"；
 * @author Javen
 */
public class ClassLoaderTest {
    public static ClassLoaderTest t1 = new ClassLoaderTest();
    public static ClassLoaderTest t2 = new ClassLoaderTest();

    {
        System.out.println("构造块");
    }

    static {
        System.out.println("静态块");
    }

    public static void main(String[] args) {
        ClassLoaderTest t = new ClassLoaderTest();
        /**
         * 输出结果:
         * 构造块
         * 构造块
         * 静态块
         * 构造块
         */
    }
}
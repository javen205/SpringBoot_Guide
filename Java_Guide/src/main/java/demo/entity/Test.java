package demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Javen
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Test {
    private String name;
    private Integer age;
    private Object openSource;
}

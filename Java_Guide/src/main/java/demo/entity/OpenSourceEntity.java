package demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Javen
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenSourceEntity {
    private String author;
    private String name;
    private String url;
}

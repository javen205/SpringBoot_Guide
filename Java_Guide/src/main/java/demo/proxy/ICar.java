package demo.proxy;

/**
 * @author Javen
 */
public interface ICar {
    /**
     * 开起来
     */
    void run();
}

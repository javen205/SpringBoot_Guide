package demo.proxy;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Javen
 */
@Slf4j
public class Tesla implements ICar{
    @Override
    public void run() {
        log.info("Tesla 跑起来了....");
    }
}

package demo.proxy;

import java.util.Date;

/**
 * @author Javen
 */
public class ShallowCopy implements Cloneable {
    private Date begin;

    public Date getBegin() {
        return this.begin;
    }

    public void setBegin(Date d) {
        this.begin = d;
    }

    public Object clone() {
        ShallowCopy obj = null;
        try {
            obj = (ShallowCopy) super.clone();
//            obj.setBegin((Date)this.getBegin().clone());
        } catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return obj;
    }

    public static void main(String[] args) {
        Date date = new Date(10000L);
        ShallowCopy copy = new ShallowCopy();
        copy.setBegin(date);
        ShallowCopy copy2 = (ShallowCopy) copy.clone();
        System.out.println(copy.getBegin() + "\n"
                + copy2.getBegin() + "\n" +
                (copy == copy2));
        date.setTime(100000000L);
        System.out.println(copy.getBegin() + "\n"
                + copy2.getBegin() + "\n" +
                (copy == copy2));

        /**
         * Thu Jan 01 08:00:10 CST 1970
         * Thu Jan 01 08:00:10 CST 1970
         * false
         * Fri Jan 02 11:46:40 CST 1970
         * Thu Jan 01 08:00:10 CST 1970
         * false
         */
    }
}
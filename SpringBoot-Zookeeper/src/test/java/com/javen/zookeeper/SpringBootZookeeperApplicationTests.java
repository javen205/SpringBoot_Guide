package com.javen.zookeeper;

import com.javen.zookeeper.order.OrderGenerator;
import com.javen.zookeeper.order.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.data.Stat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.concurrent.*;

@SpringBootTest
@Slf4j
class SpringBootZookeeperApplicationTests {
    @Autowired
    ZookeeperApi zookeeperApi;

    @Test
    public void zookeeper() {
        try {
            String path = "/zk-test";
            String openPath = "/open";
            Stat stat = zookeeperApi.exists(path, false);
            log.info(String.format("%s 是否存在 %s", path, stat == null));
            boolean created = zookeeperApi.createNode(path, "{\"IJPay\":\"https://gitee.com/javen205/IJPay\"}");
            log.info(String.format("是否创建成功 %s", created));
            String value = zookeeperApi.getData(path, new ZookeeperWatcher());
            log.info(String.format("getData value %s", value));
            boolean updated = zookeeperApi.updateNode(path, "{\"IJPay\":\"https://gitee.com/javen205/IJPay\",\"TNWX\":\"https://gitee.com/javen205/TNWX\"}");
            log.info(String.format("是否更新成功 %s", updated));


            boolean createNode = zookeeperApi.createNode(openPath, "{\"IJPay\":\"https://gitee.com/javen205/IJPay\"}");
            log.info(String.format("是否创建成功 %s", createNode));
            List<String> list = zookeeperApi.getChildren(path);
            log.info(String.format("子节点 %s", list));

            // 删除节点触发监听事件
            boolean delete = zookeeperApi.deleteNode(openPath);
            log.info(String.format("是否删除成功 %s", delete));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createNodeByEphemeral() {
        // 临时节点创建成功后，断开连接后会自动删除
        boolean created = zookeeperApi.createNodeByEphemeral("/createNodeByEphemeral", "");
        System.out.println(created);

        String data = zookeeperApi.getData("/createNodeByEphemeral", new ZookeeperWatcher());
        System.out.println(data);
    }
}

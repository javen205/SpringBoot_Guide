package com.javen.zookeeper;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

/**
 * @author Javen
 */
@Slf4j
public class ZookeeperWatcher implements Watcher {
    @Override
    public void process(WatchedEvent event) {
        log.info(String.format("【Watcher监听事件】%s", event.getState()));
        log.info(String.format("【监听路径为】%s", event.getPath()));
        //  三种监听类型： 创建，删除，更新
        log.info(String.format("【监听的类型为】%s", event.getType()));
    }
}

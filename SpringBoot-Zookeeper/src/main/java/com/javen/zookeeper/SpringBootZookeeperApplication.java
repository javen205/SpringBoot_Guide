package com.javen.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Javen
 */
@SpringBootApplication
public class SpringBootZookeeperApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootZookeeperApplication.class, args);
    }

}

package com.javen.zookeeper.order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.*;

/**
 * @author Javen
 */
public class OrderGenerator {
    private static int count = 0;

    private static final ThreadLocal<DateFormat> DF = ThreadLocal.withInitial(() ->
            new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss"));

    public String getNumber() {
        try {
            Thread.sleep(300);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DF.get().format(new Date()) + "-" + ++count;
    }
}


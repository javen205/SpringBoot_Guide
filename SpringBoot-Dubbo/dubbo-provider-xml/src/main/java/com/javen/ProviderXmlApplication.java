package com.javen;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.container.Main;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Javen
 */
@Slf4j
@SpringBootApplication
@ImportResource(locations = "classpath:provider.xml")
@EnableHystrix
public class ProviderXmlApplication{

    public static void main(String[] args) {
        SpringApplication.run(ProviderXmlApplication.class, args);
        Main.main(args);
    }


}

package com.javen.service.impl;

import com.javen.HelloService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Javen
 */
@Slf4j
@Component
public class HelloServiceImpl implements HelloService {
    @Value("${server.port}")
    String port;

    @HystrixCommand
    @Override
    public String sayHello(String name) {
        String tempStr = "sayHello_3";

        log.info("xml provider sayHello name:{} ", name);

        if (tempStr.equals(name)){
            throw new RuntimeException("provider exception...");
        }
        
        return port + ":Hello " + name;
    }
}
package com.javen;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Javen
 */
@SpringBootApplication
@ImportResource(locations = "classpath:consumer.xml")
@EnableHystrix
public class ConsumerXmlApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerXmlApplication.class, args);
    }

}

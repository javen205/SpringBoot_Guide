package com.javen.controller;

import com.javen.HelloService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Javen
 */
@Slf4j
@RestController
public class HelloController {

    @Autowired
    private  HelloService helloService;

//    @Autowired
//    public HelloController(HelloService helloService) {
//        this.helloService = helloService;
//    }

    AtomicLong atomicLong = new AtomicLong();

    @HystrixCommand(fallbackMethod = "errorCall")
    @RequestMapping("/hello")
    public String hello(@RequestParam(defaultValue = "world", value = "name", required = false) String name) {
        long id = atomicLong.getAndIncrement();
        String hello = helloService.sayHello(name + "_" + id);
        log.info("consumer call result: {}", hello);
        return hello;
    }

    public String errorCall(String name) {
        log.info("errorCall ...");
        return "出现异常了...";
    }
}
package com.javen.service.impl;

import com.javen.HelloService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Javen
 */
@Slf4j
@Component
@Service(version = "${demo.service.version}")
public class HelloServiceImpl implements HelloService {
    @Value("${server.port}")
    String port;

    /**
     * 阀值 2 秒 10 次 默认为 5 秒 20 次
     *
     * @param name 姓名
     * @return {@link String}
     */
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")
    })
    @Override
    public String sayHello(String name) {
        String tempStr = "sayHello_3";

        log.info("provider sayHello name:{} ", name);

        if (tempStr.equals(name)) {
            throw new RuntimeException("provider exception...");
        }

        return port + ":Hello " + name;
    }
}
package com.javen.impl;


import com.javen.HelloService;
import lombok.extern.slf4j.Slf4j;

/**
 * 本地存根
 * http://dubbo.apache.org/zh-cn/docs/user/demos/local-stub.html
 *
 * @author Javen
 */
@Slf4j
public class HelloServiceStub implements HelloService {
    private final HelloService helloService;

    /**
     * 构造函数传入真正的远程代理对象
     *
     * @param helloService {@link HelloService}
     */
    public HelloServiceStub(HelloService helloService) {
        this.helloService = helloService;
    }

    @Override
    public String sayHello(String name) {
        String tempStr = "sayHello_0";
        log.info("本地存根,请求参数 name: {}", name);
        // 此代码在客户端执行, 你可以在客户端做ThreadLocal本地缓存，或预先验证参数是否合法，等等
        if (tempStr.equals(name)) {
            return "拦截了....";
        }
        return helloService.sayHello(name);
    }
}

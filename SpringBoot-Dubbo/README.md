## SpringBoot 集成 Dubbo


### 基于注解

- 导入 dubbo start
- 在   application.yml 配置属性，使用 @Service「暴露服务」使用 @Reference 「引用服务」
- dubbo.scan.base-packages: com.javen.service.impl 设置了包扫描就不用使用 @EnableDubbo

## 基于 xml

- 导入 dubbo start
- 保留 dubbo xml 配置，使用 @ImportResource(locations = "classpath:provider.xml")

### 版本说

- SpringBoot 2.2.7.RELEASE
- Zookeeper 3.6.0
- dubbo-spring-boot-starter apache  2.7.6

 


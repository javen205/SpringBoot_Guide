# Docker RabbitMQ 集群

```shell
$ docker run -d --hostname rabbit-1 --name rabbitmq-1 -p 15672:15672  -p 5672:5672 -e RABBITMQ_ERLANG_COOKIE='cookie-javen' rabbitmq:management

$ docker run -d --hostname rabbit-2 --name rabbitmq-2 -p 5673:5672 --link rabbitmq-1:rabbit-1 -e RABBITMQ_ERLANG_COOKIE='cookie-javen' rabbitmq:management

$ docker run -d --hostname rabbit-3 --name rabbitmq-3 -p 5674:5672 --link rabbitmq-1:rabbit-1 --link rabbitmq-2:rabbit-2 -e RABBITMQ_ERLANG_COOKIE='cookie-javen' rabbitmq:management
```

RabbitMQ的集群是依赖 Erlang 集群，而 Erlang 集群是通过 cookie 进行通信认证的，因此我们做集群的第一步就是配置 cookie


```shell
$ echo cookie-javen > .erlang.cookie
```

要想知道 Erlang Cookie 位置，首先要取得 RabbitMQ 启动日志里面的 home dir 路径，作为根路径。

```shell
$ docker logs rabbit-1
```



分别对三个容器拷贝 cookie 文件

```shell
$ docker cp .erlang.cookie rabbitmq-1(容器名):/var/lib/rabbitmq/.erlang.cookie # 拷贝 Erlang Cookie 文件至容器
$ docker exec -it rabbitmq-1(容器名) bash # 进入容器
$ chmod 600 /var/lib/rabbitmq/.erlang.cookie # 修改文件权限
```



加入 RabbitMQ 节点到集群

```shell
$ docker exec -it rabbitmq-1 bash
$ rabbitmqctl stop_app
$ rabbitmqctl reset
$ rabbitmqctl start_app
$ exit
```



```shell
$ docker exec -it rabbitmq-2 bash
$ rabbitmqctl stop_app
$ rabbitmqctl reset
$ rabbitmqctl join_cluster --ram rabbit@rabbit-1(节点名在管理端查看 Overview->Nodes)
$ rabbitmqctl start_app
$ exit
```



```shell
$ docker exec -it rabbitmq-3 bash
$ rabbitmqctl stop_app
$ rabbitmqctl reset
$ rabbitmqctl join_cluster --ram rabbit@rabbit-1
$ rabbitmqctl start_app
$ exit
```

参数 `--ram `  表示设置为内存节点，忽略次参数默认为磁盘节点。

**ram 模式不支持 rabbitmq_delayed_message_exchange 插件**

```shell
$ rabbitmqctl change_cluster_node_type {disc,ram} # 改变节点类型
```  

RabbitMQ只要求在集群中至少有一个磁盘节点，如果集群只有一个磁盘节点，凑巧它崩溃了，那么集群可以继续发送或接收消息，但不能执行创建队列、交换器、绑定关系、用户，以及更改权限、添加或删除集群节点的操作。
所以在建立集群时，至少要保证有两个以上的磁盘节点。



启用插件
```shell
$ rabbitmq-plugins enable rabbitmq_delayed_message_exchange 
$ rabbitmqctl forget_cluster_node rabbit@rabbit-2 # 剔除单个节点
```  

- rabbitmq-delayed-message-exchange 插件不支持 ram 节点

- x-dead-letter-exchange 转发到队列，顺序又有要求，不能自动排序。
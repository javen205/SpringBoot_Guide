# Docker 安装 RabbitMq

```shell script
$ docker run -d --hostname rabbit-host --name rabbitmq  -p 15672:15672 -p 5672:5672 rabbitmq:management
```

## 下载插件

找到对应版本的 rabbitmq-delayed-message-exchange

https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases

更多插件:https://www.rabbitmq.com/community-plugins.html

## 开启插件

```shell script
$ docker exec -it rabbitmq /bin/bash命令  # 进入 Docker
$ ls -l                                  # 查看插件目录
$ docker cp rabbitmq_delayed_message_exchange-3.8.0.ez rabbitmq:/plugins # copy 插件   
$ rabbitmq-plugins enable rabbitmq_delayed_message_exchange   # 再次进入 Docker 开启插件 
$ docker restart  rabbitmq # 退出容器重启 rabbitmq
```                                                                                   

## 检查插件是否开启成功

http://localhost:15672/#/exchanges

Add a new exchange 中的 type 会多一个 x-delay-message

## Spring Boot 中使用

更多细节请参考源码

```java 
public static final String DELAY_QUEUE = "delay_queue";
public static final String DELAY_KEY = "delay_key";
public static final String DELAY_EXCHANGE = "delay_exchange"

@Bean
public Queue delayQueue() {
    return new Queue(Constant.DELAY_QUEUE,true);
}  

@Bean
public CustomExchange delayExchange() {
    Map<String, Object> args = new HashMap<>();
    args.put("x-delayed-type", "direct");
    return new CustomExchange(Constant.DELAY_EXCHANGE, "x-delayed-message", true, false, args);
}

@Bean
public Binding bindingDelayExchange(Queue delayQueue, CustomExchange customExchange) {
    return BindingBuilder.bind(delayQueue).to(customExchange).with(Constant.DELAY_KEY).noargs();
}   



@Autowired
private RabbitTemplate rabbitTemplate;

@GetMapping("/sendDelayMessage")
public String sendDelayMessage(String msg, int delayTime) {
    try {
        rabbitTemplate.convertAndSend(Constant.DELAY_EXCHANGE, Constant.DELAY_KEY, msg,
                message -> {
                    // 注意这里时间可以使long，而且是设置header
                    // 设置延迟多少分钟
                    message.getMessageProperties().setHeader("x-delay", delayTime * 60000);
                    return message;
                },
                new CorrelationData(UUID.randomUUID().toString()));
    } catch (Exception e) {
        e.printStackTrace();
        return "send message failure";
    }
    return "send message success";
}
```    


## 参考资料

- [docker下安装RabbitMQ延迟队列插件](https://blog.csdn.net/magic_1024/article/details/103840681)
- [Spring Boot + RabbitMQ + rabbitmq_delayed_message_exchange插件实现延迟队列](https://blog.csdn.net/qq_33749799/article/details/88938166)
- [SpringBoot 集成 MQ 发送接收各种消息介绍](https://www.cnblogs.com/skychenjiajun/p/9037324.html)



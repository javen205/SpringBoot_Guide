
package com.javen.vo;

import java.io.Serializable;

public class Order implements Serializable {
    private static final long serialVersionUID = 6904220315071561577L;
    private String orderId;
    private int count;
    private String desc;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

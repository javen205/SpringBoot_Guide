package com.javen.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * TopicExchange 是按规则转发消息
 * 
 * TopicExchange 交换机支持使用通配符*、#
 *
 * *号只能向后多匹配一层路径。
 *
 * #号可以向后匹配多层路径。
 */
@Configuration
public class TopicConfig {
    @Bean
    public Queue coreQueue() {
        return new Queue(Constant.RABBITMQ_QUEUE_CORE);
    }
     
    @Bean
    public Queue orderQueue() {
        return new Queue(Constant.RABBITMQ_QUEUE_ORDER);
    }
     
    @Bean
    public TopicExchange coreExchange() {
        return new TopicExchange(Constant.TOPIC_EXCHANGE);
    }
     
    @Bean
    public TopicExchange orderExchange() {
        return new TopicExchange(Constant.TOPIC_EXCHANGE);
    }
     
    @Bean
    public Binding bindingCoreExchange(Queue coreQueue, TopicExchange coreExchange) {
        // 绑定消息队列以及交换机并设置路由key
        return BindingBuilder.bind(coreQueue).to(coreExchange).with(Constant.CORE_ROUTING_KEY_ORDER);
    }
     
    @Bean
    public Binding bindingOrderExchange(Queue orderQueue, TopicExchange orderExchange) {
        // 绑定消息队列以及交换机并设置路由key
        return BindingBuilder.bind(orderQueue).to(orderExchange).with(Constant.ORDER_ROUTING_KEY);
    }
}
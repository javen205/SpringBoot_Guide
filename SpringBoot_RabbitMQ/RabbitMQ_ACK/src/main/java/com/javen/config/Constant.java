package com.javen.config;

/**
 * @author Javen
 */
public class Constant {
    public final static String RABBITMQ_QUEUE_FANOUT = "fanoutQueue";
    public final static String RABBITMQ_QUEUE_ORDER = "orderQueue";
    public final static String RABBITMQ_QUEUE_CORE = "coreQueue";
    public final static String RABBITMQ_QUEUE_BANK = "bankQueue";
    public static final String DELAY_QUEUE = "delay_queue";

    /**
     * TTL(time to live) 配置在消息上的缓冲队列
     * 发送到该队列的 message 会在一段时间后过期进入到 delay_process_queue
     * 每个 message 可以控制自己的失效时间
     */
    public static final String DELAY_QUEUE_PER_MESSAGE_TTL_NAME = "delay_queue_per_message_ttl";

    /**
     * TTL 配置在队列上的缓冲队列
     * 发送到该队列的 message 会在一段时间后过期进入到 delay_process_queue
     * 队列里所有的 message 都有统一的失效时间
     */
    public static final String DELAY_QUEUE_PER_QUEUE_TTL_NAME = "delay_queue_per_queue_ttl";

    /**
     * 延迟消息实际消费队列
     * message 失效后进入的队列，也就是实际的消费队列
     */
    public static final String DELAY_PROCESS_QUEUE_NAME = "delay_process_queue";

    // * 匹配任意 1 个词 # 匹配 0 到多个词
    public final static String ORDER_ROUTING_KEY = "api.order.#";
    public static final String CORE_ROUTING_KEY_ORDER = "api.core.*";
    public static final String DELAY_KEY = "delay_key";

    public final static String FANOUT_EXCHANGE = "amq.fanout";
    public final static String TOPIC_EXCHANGE = "amq.topic";
    public final static String HEADERS_EXCHANGE = "amq.headers";
    public final static String DIRECT_EXCHANGE = "amq.direct";
    public static final String DELAY_EXCHANGE = "delay_exchange";
    public static final String DELAY_TTL_EXCHANGE = "delay_ttl_exchange";

    /**
     * 路由到 delay_queue_per_queue_ttl 的 exchange
     */
    public final static String PER_QUEUE_TTL_EXCHANGE_NAME = "per_queue_ttl_exchange";
}

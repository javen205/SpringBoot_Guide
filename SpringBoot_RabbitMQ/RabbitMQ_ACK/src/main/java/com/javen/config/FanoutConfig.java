package com.javen.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * FanoutExchange 交换机是转发消息到所有绑定队列（广播模式和 routingKey 没有关系）
 */
@Configuration
public class FanoutConfig {

    @Bean
    public Queue reportRefundQueue() {
        return new Queue(Constant.RABBITMQ_QUEUE_FANOUT);
    }
     
    @Bean
    public FanoutExchange fanoutExchange() {
         return new FanoutExchange(Constant.FANOUT_EXCHANGE);
    }
     

    @Bean
    public Binding bindingRefundExchange(Queue reportRefundQueue, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(reportRefundQueue).to(fanoutExchange);
    }
}

package com.javen.config;

import com.javen.consumer.DelayConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


@Configuration
@Slf4j
public class DelayConfig {
    // 队列过期时间 4秒
    private final static int QUEUE_EXPIRATION = 4000;

    // 创建 delay_queue_per_message_ttl 队列
    @Bean
    Queue delayQueuePerMessageTTL() {
        return QueueBuilder.durable(Constant.DELAY_QUEUE_PER_MESSAGE_TTL_NAME)
                //x-dead-letter-exchange 声明了队列里的死信转发到DLX交换机
                .withArgument("x-dead-letter-exchange", Constant.DELAY_TTL_EXCHANGE)
                //x-dead-letter-routing-key 声明了这些死信在转发时携带的routing-key名称
                .withArgument("x-dead-letter-routing-key", Constant.DELAY_PROCESS_QUEUE_NAME)
                .build();


          // 等同于
//        Map<String,Object> parms = new HashMap<>();
//        //x-dead-letter-exchange声明了队列里的死信转发到DLX交换机
//        parms.put("x-dead-letter-exchange",Constant.DELAY_TTL_EXCHANGE);
//        //x-dead-letter-routing-key 声明了这些死信在转发时携带的routing-key名称
//        parms.put("x-dead-letter-routing-key",Constant.DELAY_PROCESS_QUEUE_NAME);
//        return new Queue(Constant.DELAY_QUEUE_PER_MESSAGE_TTL_NAME,true,false,false,parms);
    }

    // 创建 delay_queue_per_queue_ttl 队列
    @Bean
    Queue delayQueuePerQueueTTL() {
        return QueueBuilder.durable(Constant.DELAY_QUEUE_PER_QUEUE_TTL_NAME)
                .withArgument("x-dead-letter-exchange", Constant.DELAY_TTL_EXCHANGE)
                .withArgument("x-dead-letter-routing-key", Constant.DELAY_PROCESS_QUEUE_NAME)
                // 设置队列的过期时间
                .withArgument("x-message-ttl", QUEUE_EXPIRATION)
                .build();
    }

    // 创建 delay_process_queue 队列，也就是实际消费队列
    @Bean
    Queue delayProcessQueue() {
        return QueueBuilder.durable(Constant.DELAY_PROCESS_QUEUE_NAME)
                .build();
    }

    @Bean
    DirectExchange delayExchange() {
        return new DirectExchange(Constant.DELAY_TTL_EXCHANGE);
    }

    // 创建 per_queue_ttl_exchange
    @Bean
    DirectExchange perQueueTTLExchange() {
        return new DirectExchange(Constant.PER_QUEUE_TTL_EXCHANGE_NAME);
    }
    
    // 将 DLX 绑定到实际消费队列
    @Bean
    Binding dlxBinding(Queue delayProcessQueue, DirectExchange delayExchange) {
        return BindingBuilder.bind(delayProcessQueue)
                .to(delayExchange)
                .with(Constant.DELAY_PROCESS_QUEUE_NAME);
    }

    // 将 per_queue_ttl_exchange 绑定到 delay_queue_per_queue_ttl 队列
    @Bean
    Binding queueTTLBinding(Queue delayQueuePerQueueTTL, DirectExchange perQueueTTLExchange) {
        return BindingBuilder.bind(delayQueuePerQueueTTL)
                .to(perQueueTTLExchange)
                .with(Constant.DELAY_QUEUE_PER_QUEUE_TTL_NAME);
    }
}

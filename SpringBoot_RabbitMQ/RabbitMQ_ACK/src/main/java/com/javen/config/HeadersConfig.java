package com.javen.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * HeadersExchange 交换机是根据请求消息中设置的header attribute参数类型来匹配的,和routingKey没有关系
 */
@Configuration
public class HeadersConfig {

    @Bean
    public Queue bankQueue() {
        return new Queue(Constant.RABBITMQ_QUEUE_BANK);
    }
     
    @Bean
    public HeadersExchange headersExchange() {
         return new HeadersExchange(Constant.HEADERS_EXCHANGE);
    }

    @Bean
    public Binding bindingBankExchange(Queue bankQueue, HeadersExchange headersExchange) {
        Map<String,Object> headerValues = new HashMap<>();
        headerValues.put("type", "cash");
        headerValues.put("name", "china");
        // whereAll 匹配所有
        // whereAny 匹配任何一个
        return BindingBuilder.bind(bankQueue).to(headersExchange).whereAll(headerValues).match();
    }
}
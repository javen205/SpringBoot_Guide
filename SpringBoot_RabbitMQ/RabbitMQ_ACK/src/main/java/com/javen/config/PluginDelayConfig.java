
package com.javen.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
public class PluginDelayConfig {
    @Bean
    public Queue pluginDelayQueue() {
        return new Queue(Constant.DELAY_QUEUE,true);
    }

    @Bean
    public CustomExchange pluginDelayExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(Constant.DELAY_EXCHANGE, "x-delayed-message", true, false, args);
    }

    @Bean
    public Binding bindingDelayExchange(Queue pluginDelayQueue, CustomExchange pluginDelayExchange) {
        return BindingBuilder.bind(pluginDelayQueue).to(pluginDelayExchange).with(Constant.DELAY_KEY).noargs();
    }
}


package com.javen.sender;

import com.javen.config.Constant;
import com.javen.config.ExpirationMessagePostProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

@Slf4j
@Component
public class RabbitSender {
//    @Autowired
//    private RabbitTemplate rabbitTemplate;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public RabbitSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void pluginDelayMsg(Object msg, int delayTime) {
        log.info("send delay message:{}, delayTime:{}", msg, delayTime);
        rabbitTemplate.convertAndSend(Constant.DELAY_EXCHANGE, Constant.DELAY_KEY, msg,
                message -> {
                    // 注意这里时间可以使long，而且是设置header
                    // 设置延迟多少分钟
                    message.getMessageProperties().setHeader("x-delay", delayTime * 60000);
                    return message;
                },
                new CorrelationData(UUID.randomUUID().toString()));
    }

    public void delayQueuePerMessageTTL(Object msg, long expiration) {
        rabbitTemplate.convertAndSend(Constant.DELAY_QUEUE_PER_MESSAGE_TTL_NAME, msg, new ExpirationMessagePostProcessor(expiration));
    }

    public void delayQueuePerQueueTTL(Object msg) {
        rabbitTemplate.convertAndSend(Constant.DELAY_QUEUE_PER_QUEUE_TTL_NAME, msg);
    }

    public void order(Object msg) {
        log.info("api.order send message:{} ", msg);
        rabbitTemplate.convertAndSend(Constant.TOPIC_EXCHANGE, "api.order", msg);
    }

    public void orderQuery(Object msg) {
        log.info("api.order.query send message: {} ", msg);
        rabbitTemplate.convertAndSend(Constant.TOPIC_EXCHANGE, "api.order.query", msg);
    }

    public void orderDetailQuery(Object msg) {
        log.info("api.order.detail.query send message: {} ", msg);
        rabbitTemplate.convertAndSend(Constant.TOPIC_EXCHANGE, "api.order.detail.query", msg);
    }

    public void user(String msg) {
        log.info("api.core.user send message: {} ", msg);
        rabbitTemplate.convertAndSend(Constant.TOPIC_EXCHANGE, "api.core.user", msg);
    }

    public void userQuery(String msg) {
        log.info("api.core.user.query send message: {} ", msg);
        rabbitTemplate.convertAndSend(Constant.TOPIC_EXCHANGE, "api.core.user.query", msg);
    }

    public void fanout(String msg) {
        log.info("fanout send message: {} ", msg);
        rabbitTemplate.convertAndSend(Constant.FANOUT_EXCHANGE, "", msg);
    }

    public void headersMsg(Map<String, Object> head, String msg) {
        log.info("headers send message:: {} ", msg);
        rabbitTemplate.convertAndSend(Constant.HEADERS_EXCHANGE, "", getMessage(head, msg));
    }

    private Message getMessage(Map<String, Object> head, Object msg) {
        MessageProperties messageProperties = new MessageProperties();
        for (Map.Entry<String, Object> entry : head.entrySet()) {
            messageProperties.setHeader(entry.getKey(), entry.getValue());
        }
        MessageConverter messageConverter = new SimpleMessageConverter();
        return messageConverter.toMessage(msg, messageProperties);
    }
}

package com.javen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Javen
 */
@SpringBootApplication
public class AckApplication {

    public static void main(String[] args) {
        SpringApplication.run(AckApplication.class, args);
    }
}

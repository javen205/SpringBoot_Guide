package com.javen.consumer;

import com.javen.config.Constant;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


/**
 * @author Javen
 */
@Component
@RabbitListener(queues = {Constant.DELAY_QUEUE})
@Slf4j
public class PluginDelayConsumer {
    @RabbitHandler
    public void consumeMessage(String msg, Message message, Channel channel) {
        try {
            log.info("PluginDelayConsumer 消费者 msg:{}, message:{}, channel:{}", msg, message, channel);
            // 确定被消费
             channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            // 拒绝消息并重新返回到队列
//            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
            log.info("PluginDelayConsumer receiver success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
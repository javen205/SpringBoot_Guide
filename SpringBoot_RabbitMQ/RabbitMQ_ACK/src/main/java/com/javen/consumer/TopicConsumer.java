package com.javen.consumer;

import com.javen.config.Constant;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TopicConsumer {

    @RabbitHandler
    @RabbitListener(queues = {Constant.RABBITMQ_QUEUE_ORDER})
    public void consumeOrderMessage(String msg, Message message, Channel channel) {
        try {
            log.info("order 队列 msg: {} message:{} channel: {}", msg, message, channel);
            // 确定被消费
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            // 拒绝消息并重新返回到队列
            // channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
            log.info("order receiver success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RabbitHandler
    @RabbitListener(queues = {Constant.RABBITMQ_QUEUE_CORE})
    public void consumeCoreMessage(String msg, Message message, Channel channel) {
        try {
            log.info("core 队列 msg: {} message:{} channel: {}", msg, message, channel);
            // 确定被消费
             channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            // 拒绝消息并重新返回到队列
//            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            log.info("core receiver success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RabbitHandler
    @RabbitListener(queues = {Constant.RABBITMQ_QUEUE_FANOUT})
    public void consumeFanoutMessage(String msg, Message message, Channel channel) {
        try {
            log.info("fanout 队列 msg: {} message:{} channel: {}", msg, message, channel);
            // 确定被消费
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            // 拒绝消息并重新返回到队列
//            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            log.info("fanout receiver success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
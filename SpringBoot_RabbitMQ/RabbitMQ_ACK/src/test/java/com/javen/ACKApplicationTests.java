package com.javen;

import com.javen.sender.RabbitSender;
import com.javen.vo.JsonResult;
import com.javen.vo.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ACKApplicationTests {
    @Autowired
    private RabbitSender sender;

    @Test
    public void sendPluginDelayMsg() {
        for (int i = 0; i < 2; i++) {
            sender.pluginDelayMsg("delay " + i, 2 - i);
        }
    }

    @Test
    public void sendDelayMsg() {
        for (int i = 0; i < 2; i++) {
            Order order = new Order();
            order.setOrderId(UUID.randomUUID().toString());
            order.setCount(1);
            order.setDesc("延迟消息" + i);
            sender.delayQueuePerMessageTTL(order, 60000 * (2 - i)); // 过期时间 1 分钟
        }
    }

    @Test
    public void sendDelayQueueMsg() {
        Order order = new Order();
        order.setOrderId(UUID.randomUUID().toString());
        order.setCount(1);
        order.setDesc("延迟消息队列");
        sender.delayQueuePerQueueTTL(order);
    }

    @Test
    public void order() {
        sender.order("订单管理...");
    }

    @Test
    public void orderQuery() {
        sender.orderQuery("查询订单信息...");
    }

    @Test
    public void orderDetailQuery() {
        sender.orderDetailQuery("查询订单详情信息...");
    }

    @Test
    public void user() {
        sender.user("用户管理...");
    }

    @Test
    public void userQuery() {
        sender.userQuery("查询用户信息...");
    }

    @Test
    public void bank() {
        Map<String, Object> head = new HashMap<>();
        head.put("type", "cash");
        head.put("name", "china");
        sender.headersMsg(head, "银联前置(全匹配)");
    }

    @Test
    public void bank2() {
        Map<String, Object> head = new HashMap<>();
        head.put("type", "cash");
        sender.headersMsg(head, "银联前置(部分匹配)");
    }

    @Test
    public void fanout() {
        sender.fanout("fanout msg");
    }
}

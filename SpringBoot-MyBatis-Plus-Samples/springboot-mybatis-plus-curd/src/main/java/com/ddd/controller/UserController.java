package com.ddd.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ddd.entity.User;
import com.ddd.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private IUserService iUserService;

    @GetMapping("/getAll")
    public List<User> getAll(){
        return iUserService.getAll();
    }

    @GetMapping("/getUser")
    public User getUser(){
        return iUserService.getUser("Jack");
    }

    @GetMapping(value = "/getUserByPage")
    public IPage<User> getUserByPage(@RequestParam("pageNo") Integer pageNo,@RequestParam("pageSize") Integer pageSize){
        log.debug("pageNo>"+pageNo);
        log.debug("pageSize>"+pageSize);
        Page<User> page = new Page<>(pageNo, pageSize);
        return iUserService.selectPage(page,0);
    }

    @GetMapping(value = "/save")
    public boolean save(){
        User user = new User();
        user.setAge(18);
        user.setCity("HK");
        user.setEmail("123@gmail.com");
        user.setGender(1);
        user.setName("test");
        return iUserService.save(user);
    }
}


package com.ddd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ddd.entity.Test;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 测试 Mapper 接口
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
@Repository
public interface TestMapper extends BaseMapper<Test> {

}

package com.ddd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ddd.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
    User getUserByName(@Param("name") String name);
    IPage<User> selectPageVo(Page page, @Param("gender") Integer gender);
}

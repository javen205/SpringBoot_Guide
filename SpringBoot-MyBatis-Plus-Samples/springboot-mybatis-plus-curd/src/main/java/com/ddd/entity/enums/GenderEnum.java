package com.ddd.entity.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum GenderEnum implements IEnum<Integer> {
    MALE(1,"男"),
    FEMALE(2,"女"),
    UNKNOWN(0,"未知");

    private final int gender;
    private final String desc;

    GenderEnum(final int gender, final String desc) {
        this.gender = gender;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.gender;
    }

    @JsonValue
    public String getDesc(){
        return  this.desc;
    }
}

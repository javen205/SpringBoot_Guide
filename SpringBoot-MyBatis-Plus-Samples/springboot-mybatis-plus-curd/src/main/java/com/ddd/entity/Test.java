package com.ddd.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 测试
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Test extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private Integer age;

    private Integer sex;


}

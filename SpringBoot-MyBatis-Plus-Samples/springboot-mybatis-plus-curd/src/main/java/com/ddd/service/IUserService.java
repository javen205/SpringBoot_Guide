package com.ddd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ddd.entity.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
public interface IUserService extends IService<User> {
    List<User> getAll();
    User getUser(String name);
    IPage<User> selectPage(Page page, Integer gender);
}

package com.ddd.service.impl;

import com.ddd.entity.Test;
import com.ddd.mapper.TestMapper;
import com.ddd.service.ITestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试 服务实现类
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

}

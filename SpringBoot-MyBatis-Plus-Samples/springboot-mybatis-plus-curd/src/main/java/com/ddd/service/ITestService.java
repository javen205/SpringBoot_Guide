package com.ddd.service;

import com.ddd.entity.Test;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 测试 服务类
 * </p>
 *
 * @author DDD
 * @since 2019-11-14
 */
public interface ITestService extends IService<Test> {

}

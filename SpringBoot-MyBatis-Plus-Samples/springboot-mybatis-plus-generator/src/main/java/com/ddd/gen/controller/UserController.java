package com.ddd.gen.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@RestController
@RequestMapping("/user")
public class UserController {

}


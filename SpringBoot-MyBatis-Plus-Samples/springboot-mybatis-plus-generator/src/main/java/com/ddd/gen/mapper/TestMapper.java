package com.ddd.gen.mapper;

import com.ddd.gen.entity.Test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 测试 Mapper 接口
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface TestMapper extends BaseMapper<Test> {

}

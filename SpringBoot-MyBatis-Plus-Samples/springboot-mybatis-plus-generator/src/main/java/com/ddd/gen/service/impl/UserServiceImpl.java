package com.ddd.gen.service.impl;

import com.ddd.gen.entity.User;
import com.ddd.gen.mapper.UserMapper;
import com.ddd.gen.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}

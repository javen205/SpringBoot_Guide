package com.ddd.gen.mapper;

import com.ddd.gen.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface UserMapper extends BaseMapper<User> {

}

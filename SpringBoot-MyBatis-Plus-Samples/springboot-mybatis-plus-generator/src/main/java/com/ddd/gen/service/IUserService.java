package com.ddd.gen.service;

import com.ddd.gen.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface IUserService extends IService<User> {

}

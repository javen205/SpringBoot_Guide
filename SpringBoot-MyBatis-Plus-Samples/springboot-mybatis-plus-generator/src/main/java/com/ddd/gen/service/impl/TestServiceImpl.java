package com.ddd.gen.service.impl;

import com.ddd.gen.entity.Test;
import com.ddd.gen.mapper.TestMapper;
import com.ddd.gen.service.ITestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试 服务实现类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

}

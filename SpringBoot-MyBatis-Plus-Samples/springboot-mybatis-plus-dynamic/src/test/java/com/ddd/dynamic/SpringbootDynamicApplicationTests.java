package com.ddd.dynamic;

import com.ddd.dynamic.entity.User;
import com.ddd.dynamic.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

@SpringBootTest
class SpringbootDynamicApplicationTests {

    @Autowired
    private IUserService userService;

    private Random random = new Random();


    @Test
    void contextLoads() {
    }

    @Test
    public void testAddUser() {
        User user = new User();
        user.setName("测试用户" + random.nextInt());
        user.setAge(random.nextInt(100));
        userService.addUser(user);
    }

    @Test
    public void testSelectUser() {
        List<User> list = userService.list(null);
        System.out.println(list);
    }

    @Test
    public void testSelectUser2() {
        List<User> list = userService.selectUser();
        System.out.println(list);
    }
}

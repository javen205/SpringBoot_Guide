package com.ddd.dynamic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ddd.dynamic.entity.Test;

/**
 * <p>
 * 测试 Mapper 接口
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface TestMapper extends BaseMapper<Test> {

}

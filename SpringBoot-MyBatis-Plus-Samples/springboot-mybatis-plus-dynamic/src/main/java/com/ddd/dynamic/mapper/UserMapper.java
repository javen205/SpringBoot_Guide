package com.ddd.dynamic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ddd.dynamic.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface UserMapper extends BaseMapper<User> {

}

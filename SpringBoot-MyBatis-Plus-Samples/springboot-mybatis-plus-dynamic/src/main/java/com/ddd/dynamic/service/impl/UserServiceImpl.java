package com.ddd.dynamic.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ddd.dynamic.entity.User;
import com.ddd.dynamic.mapper.UserMapper;
import com.ddd.dynamic.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@Service
@DS("slave")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    @DS("master")
    public int addUser(User user) {
        return baseMapper.insert(user);
    }

    @Override
    public List<User> selectUser() {
        return baseMapper.selectList(null);
    }
}

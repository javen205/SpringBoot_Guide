package com.ddd.dynamic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
@MapperScan("com.ddd.dynamic.mapper")
public class SpringbootDynamicApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDynamicApplication.class, args);
    }

}

package com.ddd.dynamic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ddd.dynamic.entity.Test;

/**
 * <p>
 * 测试 服务类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface ITestService extends IService<Test> {

}

package com.ddd.dynamic.controller;


import com.ddd.dynamic.entity.User;
import com.ddd.dynamic.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService iUserService;

    @GetMapping("/getAll")
    public List<User> getAll(){
        return iUserService.selectUser();
    }

    @GetMapping("/save")
    public int save(@RequestParam("age") int age,@RequestParam("name") String name){
        User user = new User();
        user.setAge(age);
        user.setName(name);
        return iUserService.addUser(user);
    }
}


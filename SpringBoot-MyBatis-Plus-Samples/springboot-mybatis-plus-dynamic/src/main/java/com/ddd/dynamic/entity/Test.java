package com.ddd.dynamic.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 测试
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Test extends BaseEntity {

    private static final long serialVersionUID=1L;

    private String name;

    private Integer age;

    private Integer sex;


}

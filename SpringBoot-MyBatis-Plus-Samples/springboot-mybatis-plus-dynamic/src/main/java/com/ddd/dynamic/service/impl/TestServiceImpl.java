package com.ddd.dynamic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ddd.dynamic.entity.Test;
import com.ddd.dynamic.mapper.TestMapper;
import com.ddd.dynamic.service.ITestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试 服务实现类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

}

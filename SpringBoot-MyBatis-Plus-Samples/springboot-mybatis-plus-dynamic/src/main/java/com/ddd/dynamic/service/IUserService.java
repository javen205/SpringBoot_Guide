package com.ddd.dynamic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ddd.dynamic.entity.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DDD
 * @since 2019-11-23
 */
public interface IUserService extends IService<User> {
    int addUser(User user);
    List<User> selectUser();
}

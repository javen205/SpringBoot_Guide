# SpringCloud

### 核心依赖

| 依赖                 | 版本号        |
| -------------------- | ------------- |
| spring-boot          | 2.2.5.RELEASE |
| spring-cloud         | Hoxton.SR4    |
| spring-cloud-alibaba | 2.1.0.RELEASE |

### 项目模块说明

| 项目名称                       | 端口号     | 备注                                      |
| ------------------------------ | ---------- | ----------------------------------------- |
| cloud-eureka-server            | 7001、7002 | Eureka 服务端，默认为 7001 单例           |
| cloud-config-center            | 5566       | 配置中心 + Spring Cloud Bus               |
| cloud-config-client            | 5577、5588 | 配置中心客户端并整合消息总线Bus           |
| cloud-provider-order           | 8001、8002 | Eureka 订单服务提供者                     |
| cloud-consumer-order           | 80         | Eureka 订单服务消费者 RestTemplate+Ribbon |
| cloud-consumer-fegin-order     | 81         | Eureka 订单服务消费者 openFegin + Hystrix |
| cloud-hystrix-dashboard        | 6001       | Hystrix 图形化监控                        |
| cloud-stream-rabbitmq-provider | 6601       | Spring Cloud 消息驱动提供者               |
| cloud-stream-rabbitmq-consumer | 6602、6603 | Spring Cloud 消息驱动消费者               |
| cloud-alibaba-nacos-config     | 3377       | Nacos 配置中心                            |
| cloud-alibaba-provider-order   | 9001、9002 | Nacos 订单服务提供者                      |
| cloud-alibaba-consumer-order   | 91         | Nacos 订单服务消费者                      |


### 环境搭建

[基于 Docker 搭建依赖环境](https://gitee.com/javen205/Javen205)

- 查看状态 http://localhost:8001/actuator/health 

### 多配置环境切换

- 命令行：--spring.profiles.active=dev
- 配置文件：spring.profiles.active=dev
- JVM 参数: -Dspring.profiles.active=dev
- IDEA Active profiles: dev
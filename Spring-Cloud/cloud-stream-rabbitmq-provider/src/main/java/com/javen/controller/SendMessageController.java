package com.javen.controller;

import cn.hutool.core.util.IdUtil;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Javen
 */
@RestController
@EnableBinding(Source.class)
public class SendMessageController {
    @Resource
    private MessageChannel output;

    @GetMapping(value = "/sendMessage")
    public boolean sendMessage() {
        String id = IdUtil.fastSimpleUUID();
        return output.send(MessageBuilder.withPayload(id).build());
    }
}

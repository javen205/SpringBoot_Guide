package com.javen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Javen
 */
@SpringBootApplication
public class StreamRabbitMqProvider {
    public static void main(String[] args) {
        SpringApplication.run(StreamRabbitMqProvider.class, args);
    }
}

package com.javen.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.javen.entity.OpenSource;

/**
 * @author Javen
 */
@RestController
public class IndexController {

    private final OpenSource openSource;

    public IndexController(OpenSource openSource) {
        this.openSource = openSource;
    }

    @GetMapping("/config/getOpenSource")
    public OpenSource getConfigInfo() {
        return openSource;
    }
}

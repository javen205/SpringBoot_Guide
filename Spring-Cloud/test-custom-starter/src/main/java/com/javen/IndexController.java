package com.javen;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class IndexController {
    @Resource
    HelloService helloService;

    @RequestMapping("/")
    public String hello() {
        return helloService.sayHello("IJPay");
    }
}

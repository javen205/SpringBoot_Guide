package com.javen.service;

import com.javen.entites.CommonResult;
import com.javen.entites.Order;
import org.springframework.stereotype.Component;

/**
 * @author Javen
 * 如果提供者(provider)挂掉了，此处 Feign 可以统一处理降级
 */
@Component
public class OrderHystrixImpl implements OrderFeignInterface {
    @Override
    public CommonResult create(Order order) {
        return new CommonResult(500, "Feign Hystrix Handler create ", order);
    }

    @Override
    public CommonResult get(Long id) {
        return new CommonResult(500, "Feign Hystrix Handler get ", id);
    }

    @Override
    public CommonResult timeOut(Long id) {
        return new CommonResult(500, "Feign Hystrix Handler timeout ", id);
    }
}

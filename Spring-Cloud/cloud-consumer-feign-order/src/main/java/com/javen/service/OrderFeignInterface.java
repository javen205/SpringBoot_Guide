package com.javen.service;

import com.javen.entites.CommonResult;
import com.javen.entites.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Javen
 * 类似 Dubbo
 */
@FeignClient(value = "CLOUD-PROVIDER-ORDER", fallback = OrderHystrixImpl.class)
public interface OrderFeignInterface {
    /**
     * 创建订单
     *
     * @param order {@link Order}
     * @return {@link CommonResult}
     */
    @PostMapping(value = "/order/create")
    CommonResult create(@RequestBody Order order);

    /**
     * 查询订单
     *
     * @param id 编号
     * @return {@link CommonResult}
     */
    @GetMapping(value = "/order/get/{id}")
    CommonResult get(@PathVariable("id") Long id);

    /**
     * 超时测试
     *
     * @param id 编号
     * @return {@link CommonResult}
     */
    @GetMapping("/order/hystrix/timeout/{id}")
    CommonResult timeOut(@PathVariable("id") Long id);
}


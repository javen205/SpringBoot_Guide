# Hystrix Dashboard


依次启动项目

- cloud-hystrix-dashboard
- cloud-eureka-server
- cloud-provider-order
- cloud-consumer-feign-order


### 访问地址

http://127.0.0.1:6001/hystrix

### 监控地址

目前只有 `cloud-consumer-feign-order` 项目配置了 `Hystrix`

http://127.0.0.1:81/hystrix.stream


http://127.0.0.1:6001/turbine.stream?cluster=hystrix-turbine

### 参考资料

[Hystrix监控的配置详解](https://www.jianshu.com/p/b7b20fc09ca9)


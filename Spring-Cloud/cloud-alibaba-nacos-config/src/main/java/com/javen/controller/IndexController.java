package com.javen.controller;

import com.javen.entity.OpenSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Javen
 * 支持Nacos的动态刷新功能
 */
@RestController
@RefreshScope
public class IndexController {

    @Value("${open.author}")
    private String author;

    private final OpenSource openSource;

    public IndexController(OpenSource openSource) {
        this.openSource = openSource;
    }

    @GetMapping("/config/getOpenSource")
    public OpenSource getConfigInfo() {
        return openSource;
    }

    @GetMapping("/config/getAuthor")
    public String getAuthor() {
        return author;
    }

}

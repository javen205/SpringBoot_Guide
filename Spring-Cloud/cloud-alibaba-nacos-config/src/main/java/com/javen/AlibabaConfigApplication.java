package com.javen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Javen
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AlibabaConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(AlibabaConfigApplication.class, args);
    }
}

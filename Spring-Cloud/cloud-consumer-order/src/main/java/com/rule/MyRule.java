package com.rule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Javen
 * 配置客户端负载均衡策略,此类不能与 @ComponentScan 同包以及子包下
 */
@Configuration
public class MyRule {
    @Bean
    public IRule customRule() {
        // 定义为随机
        return new RandomRule();
    }
}

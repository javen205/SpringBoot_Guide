package com.javen.custom;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author Javen
 */
public interface LoadBalancer {
    /**
     * 初始化时自动获取 ServiceInstance
     *
     * @param serviceInstances 所有微服务实例
     * @return {@link ServiceInstance}
     */
    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
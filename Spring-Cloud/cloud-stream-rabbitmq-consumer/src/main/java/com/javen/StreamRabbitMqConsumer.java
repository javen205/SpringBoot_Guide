package com.javen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Javen
 */
@SpringBootApplication
public class StreamRabbitMqConsumer {
    public static void main(String[] args) {
        SpringApplication.run(StreamRabbitMqConsumer.class, args);
    }
}

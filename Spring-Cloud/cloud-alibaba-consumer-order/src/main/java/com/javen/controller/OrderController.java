package com.javen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author Javen
 */
@RestController
public class OrderController {
    private final RestTemplate restTemplate;
    
    @Value("${serviceUrl}")
    private String serviceUrl;

    @Autowired
    public OrderController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping(value = "/nacos/consumer/order/get/{id}")
    public String echo(@PathVariable Long id) {
        return restTemplate.getForObject(serviceUrl + "/nacos/order/get/" + id, String.class);
    }
}

package com.javen.demo.annotation;

import java.lang.annotation.*;

/**
 * @author Javen
 */
// 注解所修饰的对象范围
@Target({ElementType.METHOD,ElementType.FIELD})
// 用来声明注解的保留策略
@Retention(RetentionPolicy.RUNTIME)
// 表示这个注解应该被JavaDoc工具记录
@Documented
// 表示注解可以被继承
@Inherited
public @interface OpenSource {
    /**
     * 作者
     */
    String author() default "Javen";

    /**
     * 项目名称
     */
    String name() default "IJPay";

    /**
     * 项目地址
     */
    String url() default "https://gitee/javen205/IJPay";
}

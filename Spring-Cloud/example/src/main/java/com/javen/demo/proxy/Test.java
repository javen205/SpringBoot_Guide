package com.javen.demo.proxy;

public class Test {
    public static void main(String[] args) {
        Tesla tesla = new Tesla();
        ProxyInvocationHandler proxy = new ProxyInvocationHandler();
        proxy.setTarget(tesla);
        ICar obj = (ICar) proxy.getProxy();
        obj.run();
    }
}

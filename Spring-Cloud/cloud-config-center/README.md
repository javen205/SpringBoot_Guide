# Spring Cloud Config

### 手动刷新

> 当配置修改时，手动刷客户端

```shell script
curl -X POST "http://127.0.0.1:5577/actuator/refresh"
```   

### 消息总线 Bus 

> 当修改配置时，在配置中心使用 Bus 消息总线动态全局刷新（全局广播）

```shell script
curl -X POST "http://127.0.0.1:5566/actuator/bus-refresh"
```  

### 消息总线 Bus 定点刷新

```shell script
curl -X POST "http://127.0.0.1:5566/actuator/bus-refresh/cloud-config-client:5577"
```

其中 `cloud-config-client:5577`, `cloud-config-client` 为应用名称，`5577` 为该应用的端口号  

